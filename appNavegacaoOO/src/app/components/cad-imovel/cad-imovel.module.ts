import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadImovelComponent } from './cad-imovel.component';



@NgModule({
  declarations: [
    CadImovelComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CadImovelModule { }
