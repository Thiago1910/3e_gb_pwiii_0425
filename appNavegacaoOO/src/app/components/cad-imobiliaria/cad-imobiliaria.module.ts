import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadImobiliariaComponent } from './cad-imobiliaria.component';



@NgModule({
  declarations: [
    CadImobiliariaComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CadImobiliariaModule { }
