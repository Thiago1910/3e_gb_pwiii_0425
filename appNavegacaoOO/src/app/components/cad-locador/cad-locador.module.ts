import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadLocadorComponent } from './cad-locador.component';



@NgModule({
  declarations: [
    CadLocadorComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CadLocadorModule { }
