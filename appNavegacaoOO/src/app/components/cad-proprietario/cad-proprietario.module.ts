import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadProprietarioComponent } from './cad-proprietario.component';



@NgModule({
  declarations: [
    CadProprietarioComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CadProprietarioModule { }
