import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/models/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_T = "https://3000thiago1910-3egbapi0810-qjeh57e5han.ws-us77.gitpod.io/"
  private readonly URL_O = "https://3000thiago1910-3egbapi0810-qjeh57e5han.ws-us77.gitpod.io/"
  private readonly URL = this.URL_T

  constructor(
    private httpClient: HttpClient
  ) { }

  cadastrarUsuario(usuario: Usuario): Observable<any>{
    return this.httpClient.post<any>(`${this.URL}usuario`, usuario)
  }

  buscarUsuarios():Observable<any> {
    return this.httpClient.get<any>(`${this.URL}usuarios`)
  }

  buscarUsuariosCompletos():Observable<any> {
    return this.httpClient.get<any>(`${this.URL}usuarios/completos`)
  }
}
