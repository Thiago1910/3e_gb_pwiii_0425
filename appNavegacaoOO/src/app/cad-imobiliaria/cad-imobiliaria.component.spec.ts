import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadImobiliariaComponent } from './cad-imobiliaria.component';

describe('CadImobiliariaComponent', () => {
  let component: CadImobiliariaComponent;
  let fixture: ComponentFixture<CadImobiliariaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadImobiliariaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadImobiliariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
