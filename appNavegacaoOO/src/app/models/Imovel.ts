export class Imovel {
    private _endereco : String; 
    private _cidade : String ;
    private _bairro : String;
    private _cep : String ;
    private _uf : String ;
    private _qtdQuartos : Number ;
    private _qtdSalas : Number ;
    private _qtdBanheiros : Number ;
    private _qtdCozinhas : Number ;
    private _andares : Number;
    private _valorVenal : Number;
    private _valorLocacao : Number; 
    private _situacao : String;
    private _complemento : String;
    private _isLocavel : Boolean;
    private _isVenal : Boolean;

    constructor (endereco  : String, cidade : String, bairro : String, cep : String, uf : String, qtdQuartos : Number, qtdSalas: Number, qtdBanheiros:Number, qtdCozinhas:Number, andares:Number, valorVenal: Number, valorLocacao: Number, situacao: String, complemento: String, islocavel : Boolean, isVenal : Boolean  ) {
        this._endereco= endereco 
        this._bairro = bairro 
        this._cep = cep 
        this._cidade = cidade 
        this._andares = andares 
        this._uf = uf 
        this._qtdQuartos = qtdQuartos 
        this._qtdCozinhas = qtdCozinhas 
        this._qtdBanheiros = qtdBanheiros 
        this._qtdSalas = qtdSalas 
        this._valorVenal = valorVenal 
        this._valorLocacao = valorLocacao 
        this._situacao = situacao 
        this._complemento = complemento 
        this._isVenal = isVenal
        this._isLocavel = islocavel
    }
    public set cidade(cidade: String){
        this._cidade = cidade;
      }
  
    public get cidade(): String{
      return this._cidade;
    }
    public set endereco(endereco: String){
        this._endereco = endereco;
      }
  
    public get endereco(): String{
      return this._endereco;
    }
    public set bairro(bairro: String){
        this._bairro = bairro;
      }
  
    public get bairro(): String{
      return this._bairro;
    }
    public set cep(cep: String){
        this._cep = cep;
      }
  
    public get cep(): String{
      return this._cep;
    }
    public set uf(uf: String){
        this._uf = uf;
      }
  
    public get uf(): String{
      return this._uf;
    }
    public set qtdQuartos(qtdQuartos: Number){
        this._qtdQuartos = qtdQuartos;
      }
  
    public get qtdQuartos(): Number{
      return this._qtdQuartos;
    }
    public set qtdCozinhas(qtdCozinhas: Number){
        this._qtdCozinhas = qtdCozinhas;
      }
  
    public get qtdCozinhas(): Number{
      return this._qtdCozinhas;
    }
    public set qtdBanheiros(qtdBanheiros: Number){
        this._qtdBanheiros = qtdBanheiros;
      }
  
    public get qtdBanheiros(): Number{
      return this._qtdBanheiros;
    }
    public set qtdSalas(qtdSalas: Number){
        this._qtdSalas = qtdSalas;
      }
  
    public get qtdSalas(): Number{
      return this._qtdSalas;
    }
    public set andares(andares: Number){
        this._andares = andares;
      }
  
    public get andares(): Number{
      return this._andares;
    }
    public set valorVenal(valorVenal: Number){
        this._valorVenal = valorVenal;
      }
  
    public get valorVenal(): Number{
      return this._valorVenal;
    }
    public set valorLocacao(valorLocacao: Number){
        this._valorLocacao = valorLocacao;
      }
  
    public get valorLocacao(): Number{
      return this._valorLocacao;
    }
    public set situacao(situacao: String){
        this._situacao = situacao;
      }
  
    public get situacao(): String{
      return this._situacao
    }
    public set complemento(complemento: String){
        this._complemento = complemento;
      }
  
    public get complemento(): String{
      return this._complemento;
    }
    public set isLocavel(isLocavel: Boolean){
        this._isLocavel = isLocavel;
      }
  
    public get isLocavel(): Boolean{
      return this._isLocavel;
    }
}