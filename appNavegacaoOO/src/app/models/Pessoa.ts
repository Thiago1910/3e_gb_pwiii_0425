export class Pessoa {
    id!: number
    nome!: string
    telefone!: string
    rua!: string
    numero!: string
    bairro!: string
    cep!: string
    cidade!: string
    uf!: string
    tipo!: string

    /*private _id: number
    private _nome: string
    private _telefone: string
    private _rua: string
    private _numero: string
    private _bairro: string
    private _cep: string
    private _cidade: string
    private _uf: string
    private _tipo: string

    constructor(...) {
        ...
    }*/
}